from main import syracuse_sequence, syracuse_max
import unittest


class MyTestCase(unittest.TestCase):
    def test_seq(self):
        self.assertListEqual([3, 10, 5, 16, 8, 4, 2, 1], syracuse_sequence(3))  # add assertion here

    def test_max(self):
        self.assertEqual(16, syracuse_max(3))


if __name__ == '__main__':
    unittest.main()
