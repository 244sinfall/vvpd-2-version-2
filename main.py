# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


# def print_hi(name):
#     # Use a breakpoint in the code line below to debug your script.
#     print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
#


# See PyCharm help at https://www.jetbrains.com/help/pycharm/


def syracuse_sequence(n: int) -> list[int]:
    if n == 1:
        return [1]
    if n < 1:
        return []
    sequence = [n]
    while n != 1:
        if n % 2 == 0:
            n = int(n / 2)
        else:
            n = int((n * 3) + 1)
        sequence.append(n)
    return sequence


def syracuse_max(n: int) -> int:
    seq = syracuse_sequence(n)
    if not seq:
        raise ValueError("Недопустимое число входа")
    potential_max = 1
    for num in seq:
        if num > potential_max:
            potential_max = num
    return potential_max


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    while True:
        try:
            prompt = input("Введите число для расчета сиракузской последовательности или 0 для выхода:")
            converted = int(prompt)
            if converted == 0:
                exit(0)
            print(f"Последовательность: {syracuse_sequence(converted)}")
            print(f"Максимальное число: {syracuse_max(converted)}")
        except ValueError:
            print("Введено неверное значение, попробуйте еще раз (натуральные числа, больше 1)")
